from Sommet import *
from Arete import *
from GenerateurGrapheAleatoire import *

def nombreVoisins(L):
    n=0
    for i in range(len(L)-1):
        if(L[i+1]==True):
            n += 1
    return n
            
#Classe de la matrice d'adjacence d'un graphe: contient l'ensemble des fonctions de l'exercice 1, 2 et 3 du TP
class MatriceAdjacence:

    def __init__(self):
        self.liste_sommets = []           #Liste des sommets du graphe
        self.matrice_adjacence = {}   #Matrice d'adjacence du graphe
        self.nombre_aretes = 0           #Nombre d'aretes 
        
    
    def get_sommet_par_id(self, id):            #renvoie le sommet à partir de son id
        for sommet in self.liste_sommets:
            if sommet.id == id:
                return sommet
        return Sommet(1, "nom1")
    
    def get_sommet_par_nom(self, nom):         #renvoie le sommet à partir de son nom
        for sommet in self.liste_sommets:
            if sommet.nom == nom:
                return sommet
        return Sommet(1, "nom1")

    # Exercice 1

    # Question 1
    def graphe_vide(self):                           #le graphe vide
        return MatriceAdjacence()

    @staticmethod
    def graphe_aleatoire(n, p):                   #renvoie un graphe aléatoire de n sommets avec une probabilité p: Exercice 4
        generer_graphe_aleatoire(n, p)
        return MatriceAdjacence.charger_graphe_de_fichier("donnees/output.txt")

    # Question 2
    def ajouter_sommet(self,sommet):        #ajoute le sommet en entrée à la liste des sommets
        self.liste_sommets.append(sommet)
        self.matrice_adjacence[sommet] = {}
        for i in range(len(self.liste_sommets)):
            self.matrice_adjacence[self.liste_sommets[i]][sommet] = False   #le sommet ajouté n'a initialement aucune arete en commun avec un autre
            self.matrice_adjacence[sommet][self.liste_sommets[i]] = False   #la matrice d'adjacence est symétrique

    # Question 3
    def ajouter_arete(self, sommetA, sommetB):   #ajoute l'arete de sommets A et B dans le graphe
        if self.matrice_adjacence[sommetA][sommetB] == False and self.matrice_adjacence[sommetB][sommetA] == False:
            self.matrice_adjacence[sommetA][sommetB] = True   #si l'arete en entrée n'existait pas déja dans le graphe, on la rajoute en mettant sa case dans le tableau à 1
            self.matrice_adjacence[sommetB][sommetA] = True   #on met son symétrique également à 1
            self.nombre_aretes += 1 #incrémentation du nombre d'aretes du graphe

    # Question 4
    def supprimer_arete(self, arete):  #supprime l'arete en entrée du graphe
        if self.matrice_adjacence[arete.sommetA][arete.sommetB] and self.matrice_adjacence[arete.sommetB][arete.sommetA]:
            self.matrice_adjacence[arete.sommetA][arete.sommetB] = False  #si l'arete en entrée existait déja dans le graphe, on la supprimme en mettant sa case d
            self.matrice_adjacence[arete.sommetB][arete.sommetA] = False  #on met son symétrique également à 0
            self.nombre_aretes -= 1
        else:
            return "Arete non existente"  #sinon on renvoie "Arete non existente"

    # Question 5
    def est_voisin(self, sommetA, sommetB):
        if self.matrice_adjacence[sommetA][sommetB] and self.matrice_adjacence[sommetB][sommetA]:
            return True
        return False

    def afficher(self):                                          #Affichage du graphe
        for sommet in self.liste_sommets:
            print("\t" + sommet.nom, end='')
        print("\n", end='')
        for sommetA in self.liste_sommets:
            print(sommetA.nom, end='')
            for sommetB in self.liste_sommets:
                voisin = "0"
                if self.matrice_adjacence[sommetA][sommetB]:
                    voisin = "1"
                print("\t" + voisin, end='')
            print("\n", end='')

    # Question 5
    def est_graphe_complet(self):                       #Vérifie si le graphe est complet
        nombre_sommets = len(self.liste_sommets)
        return self.nombre_aretes == nombre_sommets * (nombre_sommets - 1) / 2 #Avec n le nombre de sommets, il faut que le graphe contient n*(n-1)/2 sommets pour qu'ils soit complet  

    # Question 6
    @staticmethod
    def charger_graphe_de_fichier(nom_fichier):    #Permet de charger un graphe à partir d'un fichier
        fichier = open(nom_fichier)
        contenu = fichier.read().split("\n")
        nombre_sommets = int(contenu[0])
        graphe = MatriceAdjacence()
        for i in range(1, nombre_sommets + 1):
            couple_id_nom = contenu[i].split(' ')
            id = int(couple_id_nom[0])
            nom = couple_id_nom[1]
            sommet_courant = Sommet(id, nom)
            graphe.ajouter_sommet(sommet_courant)
        for j in range(nombre_sommets + 1, len(contenu)):
            couple_ids_sommets = contenu[j].split(' ')
            id1, id2 = int(couple_ids_sommets[0]), int(couple_ids_sommets[1])
            sommetA = graphe.get_sommet_par_id(id1)
            sommetB = graphe.get_sommet_par_id(id2)
            graphe.ajouter_arete(sommetA, sommetB)
        return graphe

    # Question 7
    @staticmethod
    def sauvegarder_graphe_dans_fichier(graphe, nom_fichier):  #Permet de sauvegarder un graphe dans un fichier

        L=[]
        fichier = open(nom_fichier, "w")
        contenu = ""
        nombre_sommets = len(graphe.liste_sommets)
        contenu += str(nombre_sommets) + "\n"
        for sommet in graphe.liste_sommets:
            contenu += str(sommet.id) + " " + sommet.nom + "\n"
        for i in range(nombre_sommets):
            sommet_courant_A = graphe.liste_sommets[i]
            for j in range(nombre_sommets):
                if j != i:
                    sommet_courant_B = graphe.liste_sommets[j]
                    L.append((sommet_courant_A,sommet_courant_B ))
                    if graphe.est_voisin(sommet_courant_A,sommet_courant_B) and ((sommet_courant_B,sommet_courant_A ) not in L):  
                        contenu += str(sommet_courant_A.id) + " " + str(sommet_courant_B.id) + "\n"
        fichier.write(contenu)

    # Exercice 2

    # Question 1
    @staticmethod
    def inclus_sommets(graphe1, graphe2, strict): #vérifie si les sommets du graphe1 sont inclus (strictement ou non avec strict) dans l'ensemble des sommets du graphe2
            noms_sommets_graphe1 = [sommet.nom for sommet in graphe1.liste_sommets]  #Liste des noms des sommets du graphe1

            noms_sommets_graphe2 = [sommet.nom for sommet in graphe2.liste_sommets]    #Liste des noms des sommets du graphe2
            for nom_sommet_graphe1 in noms_sommets_graphe1:
                if nom_sommet_graphe1 not in noms_sommets_graphe2:
                    return False                                                        #Si le nom du sommet du graphe1 n'existe pas dans la liste des noms des sommets du graphe 2, on renvoie false 

                return not strict or len(noms_sommets_graphe1) < len(noms_sommets_graphe2) #Sinon on renvoie strict ou le résultat de la comparaison des longueurs des listes de noms


    # Question 2
    @staticmethod
    def inclus_aretes(graphe1, graphe2):
        for sommet_graphe1 in graphe1.liste_sommets:
            nombre_voisins = nombreVoisins(graphe1.matrice_adjacence[sommet_graphe1])
            for voisin_sommet_graphe1 in graphe1.liste_sommets:
                if graphe1.est_voisin(sommet_graphe1, voisin_sommet_graphe1)==True and sommet_graphe1.nom != voisin_sommet_graphe1.nom:
                    sommet_eq_graphe2 = graphe2.get_sommet_par_nom(sommet_graphe1.nom)
                    voisin_sommet_eq_graphe2 = graphe2.get_sommet_par_nom(voisin_sommet_graphe1.nom)
                    if sommet_eq_graphe2.nom == "nom1" and nombre_voisins > 0:
                        return False
                    if voisin_sommet_eq_graphe2 == "nom1" or not graphe2.est_voisin(sommet_eq_graphe2, voisin_sommet_eq_graphe2):
                        return False
        return graphe1.nombre_aretes < graphe2.nombre_aretes

    # Question 3
    @staticmethod
    def est_partiel(graphe1, graphe2):
        return MatriceAdjacence.inclus_aretes(graphe1, graphe2)

    # Question 4
    @staticmethod
    def est_sous_graphe(graphe1, graphe2):
        if not MatriceAdjacence.inclus_sommets(graphe1, graphe2, True) or not MatriceAdjacence.inclus_aretes(graphe1, graphe2):
            return False
        for sommet in graphe2.liste_sommets:
            sommet_eq_graphe1 = graphe1.get_sommet_par_nom(sommet.nom)
            if sommet_eq_graphe1 != "nom1" :
                for voisin in graphe2.liste_sommets:
                    if graphe2.est_voisin(sommet,  voisin ):
                        voisin_eq_graphe_1 = graphe1.get_sommet_par_nom(voisin.nom)
                        if voisin_eq_graphe_1 != "nom1":
                            if not graphe1.est_voisin(sommet_eq_graphe1, voisin_eq_graphe_1):
                                return False
        return True

    # Question 5
    @staticmethod
    def est_sous_graphe_partiel(graphe1, graphe2):
        return MatriceAdjacence.inclus_sommets(graphe1, graphe2, True) and MatriceAdjacence.inclus_aretes(graphe1, graphe2)

    # Question 6
    @staticmethod
    def est_clique(graphe1, graphe2):
        return MatriceAdjacence.est_sous_graphe(graphe1, graphe2) and graphe1.est_graphe_complet()

    # Question 7
    @staticmethod
    def est_stable(graphe1, graphe2):
        return MatriceAdjacence.est_sous_graphe(graphe1, graphe2) and graphe1.nombre_aretes == 0

    # Exercice 3

    # Question 1
    # Ici on recueillera les resultats sous forme de matrice de distances. Plus precisement on creera un dictionnaire
    # a 2 dimensions tel que distances[sommetA][sommetB] retourne la distance entre les 2 sommets
    
    def get_distances(self):
        matrice_distances = {}
        for sommetA in self.liste_sommets:
            matrice_distances[sommetA] = {}
            for sommetB in self.liste_sommets:
                matrice_distances[sommetA][sommetB] = len(self.liste_sommets) + 1
                # On initialise la distance a la
                # taille de la liste + 1 car les distances calculees
                # seront toujours inferieures a cette valeur
        for sommet in self.liste_sommets:  # Pour chaque sommet on calcule sa distance par rapport aux autres sommets
            self.get_distances_rec(sommet, sommet, matrice_distances, [], 1)
        return matrice_distances

    # Parcours en profondeur, la distance etant egale a la profondeur de recursion 
    def get_distances_rec(self, sommet_initial, sommet_courant, matrice_distances, sommets_visites, profondeur):
        if len(sommets_visites) < len(self.liste_sommets):
            matrice_adjacence_sommet=[]
            for sommetX in self.liste_sommets:
                if MatriceAdjacence.est_voisin(self, sommetX, sommet_courant):
                    matrice_adjacence_sommet.append(sommetX) 
            for sommet_adjacent in matrice_adjacence_sommet:
                if sommet_adjacent not in sommets_visites:
                    matrice_distances[sommet_initial][sommet_adjacent] =\
                        min(profondeur, matrice_distances[sommet_initial][sommet_adjacent])
                    sommets_visites.append(sommet_adjacent)
                    self.get_distances_rec(sommet_initial, sommet_adjacent, matrice_distances, sommets_visites, profondeur + 1)
                    sommets_visites.remove(sommet_adjacent)


    def afficher_matrice_distances(self, matrice):
        for sommet in self.liste_sommets:
            print("\t" + sommet.nom, end='')
        print("\n", end='')
        for sommetA in self.liste_sommets:
            print(sommetA.nom, end='')
            for sommetB in self.liste_sommets:
                print("\t" + str(matrice[sommetA][sommetB]), end='')
            print("\n", end='')  

    def get_excentricites(self, matrice_distances):
        liste_excentricites = {}
        for sommetA in self.liste_sommets:
            excentricite = -1
            for sommetB in self.liste_sommets:
                excentricite = max(excentricite, matrice_distances[sommetA][sommetB])
                liste_excentricites[sommetA] = excentricite
        return liste_excentricites
        
    # Question 2
    def get_diametre(self, matrice_distances):
        diametre = -1
        liste_excentricites = self.get_excentricites(matrice_distances)
        for sommet in self.liste_sommets:
            diametre = max(diametre, liste_excentricites[sommet])
        return diametre   

    def get_rayon(self, matrice_distances):
        rayon = len(self.liste_sommets) + 1  # valeur majorante
        liste_excentricites = self.get_excentricites(matrice_distances)
        for sommet in self.liste_sommets:
            rayon = min(rayon, liste_excentricites[sommet])
        return rayon

    # Question 3
    def get_centres(self, matrice_distances):
        rayon = self.get_rayon(matrice_distances)
        liste_centres = []
        liste_excentricites = self.get_excentricites(matrice_distances)
        for sommet in self.liste_sommets:
            if liste_excentricites[sommet] == rayon:
                liste_centres.append(sommet)
        return len(liste_centres), liste_centres, rayon
    
    def get_degre_sommet(self, sommet):
        c = 0
        for sommetX in self.liste_sommets:
            if self.est_voisin(sommetX, sommet):
                c += 1
        degre = c
        return degre

    # Question 4
    def get_degres(self):
        degres = {}
        for sommet in self.liste_sommets:
            degres[sommet] = self.get_degre_sommet(sommet)
        return degres

    # Question 5
    def get_centres_degre(self, matrice_distances):
        _, liste_centres, _ = self.get_centres(matrice_distances)
        degre_max = -1
        nouvelle_liste_centres = []
        for centre in liste_centres:
            degre = self.get_degre_sommet(centre)
            if degre > degre_max:
                nouvelle_liste_centres = []  # on vide la liste si on avait commence a la remplir
                degre_max = degre
            if degre == degre_max:
                nouvelle_liste_centres.append(centre)
        return len(nouvelle_liste_centres), nouvelle_liste_centres, degre_max
                    



graphe = MatriceAdjacence.graphe_aleatoire(8, 0.4)
matrice_distances = graphe.get_distances()

graphe.afficher()
print("diametre : " + str(graphe.get_diametre(matrice_distances)))
print("rayon : " + str(graphe.get_rayon(matrice_distances)))
_, centres, _ = graphe.get_centres(matrice_distances)
print("centres : " + " ".join([sommet.nom for sommet in centres]))
_, centres_degre_max, deg_max = graphe.get_centres_degre(matrice_distances)
print("degre max : " + str(deg_max))
print("centres de degre max : " + " ".join([sommet.nom for sommet in centres_degre_max]))

graphe.afficher_matrice_distances(matrice_distances)

