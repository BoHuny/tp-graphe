import os
import random

#Exercice 4
def generer_graphe_aleatoire(n, p):
    file = open("donnees/output.txt", "w")
    file.write(str(n) + "\n")
    for i in range(n):
        file.write(str(i) + " id" + str(i))
        if i < n - 1:
            file.write("\n")
    for i in range(n):
        for k in range(i + 1, n):
            if satisfait(p):
                file.write("\n" + str(i) + " " + str(k))
    file.close()


def satisfait(p):
    return random.random() < p


generer_graphe_aleatoire(7, 0.2)
