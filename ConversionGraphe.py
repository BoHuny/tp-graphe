from ListeAdjacence import *
from MatriceAdjacence import *
import Arete
# Exercice 1 Question 8 
def matrice_to_liste(g):
    if isinstance(g, MatriceAdjacence):
        g_prime = ListeAdjacence()
        liste_sommets = g.liste_sommets
        matrice_adjacence = g.matrice_adjacence
        for sommet in liste_sommets:
            g_prime.ajouter_sommet(sommet)
        for debut in matrice_adjacence:
            for fin in matrice_adjacence[debut]:
                if matrice_adjacence[debut][fin]:
                    # eviter les doublons
                    if not g_prime.est_voisin(debut, fin):
                        arete = Arete.Arete(debut, fin)
                        g_prime.ajouter_arete(arete)
        return g_prime
    print("G n'est pas du bon type")
    return None


def liste_to_matrice(g):
    if isinstance(g, ListeAdjacence):
        g_prime = MatriceAdjacence()
        liste_sommets = g.liste_sommets
        liste_adjacence = g.liste_adjacence
        for sommet in liste_sommets:
            g_prime.ajouter_sommet(sommet)
        for debut in liste_adjacence:
            for fin in liste_adjacence[debut]:
                # eviter les doublons
                if not g_prime.est_voisin(debut, fin):
                    g_prime.ajouter_arete(debut, fin)
        return g_prime
    print("g n'est pas du bon type")
    return None


#graphe_liste1 = ListeAdjacence.charger_graphe_de_fichier("donnees/output.txt")
#graphe_matrice1 = liste_to_matrice(graphe_liste1)
#graphe_liste1.afficher()
#graphe_matrice1.afficher()

#graphe_matrice2 = MatriceAdjacence.charger_graphe_de_fichier("donnees/output.txt")
#graphe_liste2 = matrice_to_liste(graphe_matrice2)
#graphe_liste2.afficher()
#graphe_matrice2.afficher()
