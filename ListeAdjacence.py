from Sommet import *
from Arete import *
from GenerateurGrapheAleatoire import *


class ListeAdjacence:

    def __init__(self):
        self.liste_sommets = []
        self.liste_adjacence = {}
        self.nombre_aretes = 0

    # Exercice 1

    # Question 1
    @staticmethod
    def graphe_vide():
        return ListeAdjacence()

    @staticmethod
    def graphe_aleatoire(n, p):
        generer_graphe_aleatoire(n, p)
        return ListeAdjacence.charger_graphe_de_fichier("donnees/output.txt")

    def get_sommet_par_id(self, id):
        for sommet in self.liste_sommets:
            if sommet.id == id:
                return sommet
        return None

    def get_sommet_par_nom(self, nom):
        for sommet in self.liste_sommets:
            if sommet.nom == nom:
                return sommet
        return None

    # Question 2
    def ajouter_sommet(self, sommet):
        self.liste_sommets.append(sommet)
        self.liste_adjacence[sommet] = []

    # Question 3
    def ajouter_arete(self, arete):
        self.liste_adjacence[arete.sommetA].append(arete.sommetB)
        if arete.sommetB != arete.sommetA:
            self.liste_adjacence[arete.sommetB].append(arete.sommetA)
        self.nombre_aretes += 1

    # Question 4
    def supprimer_arete(self, arete):
        try:
            self.liste_adjacence[arete.sommetA].remove(arete.sommetB)
            self.liste_adjacence[arete.sommetB].remove(arete.sommetA)
            self.nombre_aretes -= 1
        except ValueError:
            pass

    # Question 5
    def est_voisin(self, sommetA, sommetB):
        if sommetB in self.liste_adjacence[sommetA]:
            return 1
        return 0

    def afficher(self):
        for sommet in self.liste_sommets:
            print(str(sommet.id) + "-> [" + " ".join([str(voisin.id) for voisin in self.liste_adjacence[sommet]]) + "]")

    def est_graphe_complet(self):
        nombre_sommets = len(self.liste_sommets)
        return self.nombre_aretes == nombre_sommets * (nombre_sommets - 1) / 2

    # Question 6
    @staticmethod
    def charger_graphe_de_fichier(nom_fichier):
        fichier = open(nom_fichier)
        contenu = fichier.read().split("\n")
        nombre_sommets = int(contenu[0])
        graphe = ListeAdjacence.graphe_vide()
        for i in range(1, nombre_sommets + 1):
            couple_id_nom = contenu[i].split(' ')
            id = int(couple_id_nom[0])
            nom = couple_id_nom[1]
            sommet_courant = Sommet(id, nom)
            graphe.ajouter_sommet(sommet_courant)
        for i in range(nombre_sommets + 1, len(contenu)):
            couple_ids_sommets = contenu[i].split(' ')
            sommetA = graphe.get_sommet_par_id(int(couple_ids_sommets[0]))
            sommetB = graphe.get_sommet_par_id(int(couple_ids_sommets[1]))
            arete_courante = Arete(sommetA, sommetB)
            graphe.ajouter_arete(arete_courante)
        return graphe

    # Question 7
    @staticmethod
    def sauvegarder_graphe_dans_fichier(graphe, nom_fichier):
        fichier = open(nom_fichier, "w")
        contenu = ""
        nombre_sommets = len(graphe.liste_sommets)
        contenu += str(nombre_sommets) + "\n"
        for sommet in graphe.liste_sommets:
            contenu += str(sommet.id) + " " + sommet.nom + "\n"
        for i in range(len(graphe.liste_sommets)):
            sommet_courant_A = graphe.liste_sommets[i]
            for j in range(len(graphe.liste_adjacence[sommet_courant_A])):
                sommet_courant_B = graphe.liste_adjacence[sommet_courant_A][j]
                if sommet_courant_A.id <= sommet_courant_B.id:  # pour ne pas dupliquer les aretes
                    contenu += str(sommet_courant_A.id) + " " + str(sommet_courant_B.id) + "\n"
        fichier.write(contenu)

    # Exercice 2

    # Question 1
    @staticmethod
    def inclus_sommets(graphe1, graphe2, strict):
        noms_sommets_graphe1 = [sommet.nom for sommet in graphe1.liste_sommets]
        noms_sommets_graphe2 = [sommet.nom for sommet in graphe2.liste_sommets]
        for nom_sommet_graphe1 in noms_sommets_graphe1:
            if nom_sommet_graphe1 not in noms_sommets_graphe2:
                return False
        return not strict or len(noms_sommets_graphe1) < len(noms_sommets_graphe2)

    # Question 2
    @staticmethod
    def inclus_aretes(graphe1, graphe2):
        for sommet_graphe1 in graphe1.liste_sommets:
            liste_voisins = graphe1.liste_adjacence[sommet_graphe1]
            sommet_eq_graphe2 = graphe2.get_sommet_par_nom(sommet_graphe1.nom)
            if sommet_eq_graphe2 is None and len(liste_voisins) > 0:
                return False
            for voisin_graphe1 in liste_voisins:
                voisin_eq_graphe2 = graphe2.get_sommet_par_nom(voisin_graphe1.nom)
                if voisin_eq_graphe2 is None or not graphe2.est_voisin(sommet_eq_graphe2, voisin_eq_graphe2):
                    return False
        return graphe1.nombre_aretes < graphe2.nombre_aretes

    # Question 3
    @staticmethod
    def est_partiel(graphe1, graphe2):
        return ListeAdjacence.inclus_aretes(graphe1, graphe2)

    # Question 4
    @staticmethod
    def est_sous_graphe(graphe1, graphe2):
        if not ListeAdjacence.inclus_sommets(graphe1, graphe2, True) or not ListeAdjacence.inclus_aretes(graphe1,
                                                                                                         graphe2):
            return False
        for sommet in graphe2.liste_sommets:
            sommet_eq_graphe1 = graphe1.get_sommet_par_nom(sommet.nom)
            if sommet_eq_graphe1 is not None:
                for voisin in graphe2.liste_adjacence[sommet]:
                    voisin_eq_graphe_1 = graphe1.get_sommet_par_nom(voisin.nom)
                    if voisin_eq_graphe_1 is not None:
                        if not graphe1.est_voisin(sommet_eq_graphe1, voisin_eq_graphe_1):
                            return False
        return True

    # Question 5
    @staticmethod
    def est_sous_graphe_partiel(graphe1, graphe2):
        return ListeAdjacence.inclus_sommets(graphe1, graphe2, True) and ListeAdjacence.inclus_aretes(graphe1, graphe2)

    # Question 6
    @staticmethod
    def est_clique(graphe1, graphe2):
        return ListeAdjacence.est_sous_graphe(graphe1, graphe2) and graphe1.est_graphe_complet()

    # Question 7
    @staticmethod
    def est_stable(graphe1, graphe2):
        return ListeAdjacence.est_sous_graphe(graphe1, graphe2) and graphe1.nombre_aretes == 0

    # Exercice 3

    # Question 1
    # Ici on recueillera les resultats sous forme de matrice de distances. Plus precisement on creera un dictionnaire
    # a 2 dimensions tel que distances[sommetA][sommetB] retourne la distance entre les 2 sommets
    def get_distances(self):
        matrice_distances = {}
        for sommetA in self.liste_sommets:
            matrice_distances[sommetA] = {}
            for sommetB in self.liste_sommets:
                matrice_distances[sommetA][sommetB] = len(self.liste_sommets) + 1  # On initialise la distance a la
                # taille de la liste + 1 car les distances calculees
                # seront toujours inferieures a cette valeur

        for sommet in self.liste_sommets:  # Pour chaque sommet on calcule sa distance par rapport aux autres sommets
            self.get_distances_rec(sommet, sommet, matrice_distances, [], 1)
        return matrice_distances

    # Parcours en profondeur, la distance etant egale a la profondeur de recursion
    def get_distances_rec(self, sommet_initial, sommet_courant, matrice_distances, sommets_visites, profondeur):
        if len(sommets_visites) < len(self.liste_sommets):
            liste_adjacence_sommet = self.liste_adjacence[sommet_courant]
            for sommet_adjacent in liste_adjacence_sommet:
                if sommet_adjacent not in sommets_visites:
                    matrice_distances[sommet_initial][sommet_adjacent] = \
                        min(profondeur, matrice_distances[sommet_initial][sommet_adjacent])
                    sommets_visites.append(sommet_adjacent)
                    self.get_distances_rec(sommet_initial, sommet_adjacent, matrice_distances, sommets_visites,
                                           profondeur + 1)
                    sommets_visites.remove(sommet_adjacent)

    def get_excentricites(self, matrice_distances):
        liste_excentricites = {}
        for sommetA in self.liste_sommets:
            excentricite = -1
            for sommetB in self.liste_sommets:
                excentricite = max(excentricite, matrice_distances[sommetA][sommetB])
                liste_excentricites[sommetA] = excentricite
        return liste_excentricites

    # Question 2
    def get_diametre(self, matrice_distances):
        diametre = -1
        liste_excentricites = self.get_excentricites(matrice_distances)
        for sommet in self.liste_sommets:
            diametre = max(diametre, liste_excentricites[sommet])
        return diametre

    def get_rayon(self, matrice_distances):
        rayon = len(self.liste_sommets) + 1  # valeur majorante
        liste_excentricites = self.get_excentricites(matrice_distances)
        for sommet in self.liste_sommets:
            rayon = min(rayon, liste_excentricites[sommet])
        return rayon

    # Question 3
    def get_centres(self, matrice_distances):
        rayon = self.get_rayon(matrice_distances)
        liste_centres = []
        liste_excentricites = self.get_excentricites(matrice_distances)
        for sommet in self.liste_sommets:
            if liste_excentricites[sommet] == rayon:
                liste_centres.append(sommet)
        return len(liste_centres), liste_centres, rayon

    def get_degre_sommet(self, sommet):
        degre = len(self.liste_adjacence[sommet])
        if self.est_voisin(sommet, sommet):
            degre += 1
        return degre

    # Question 4
    def get_degres(self):
        degres = {}
        for sommet in self.liste_sommets:
            degres[sommet] = self.get_degre_sommet(sommet)
        return degres

    # Question 5
    def get_centres_degre(self, matrice_distances):
        _, liste_centres, _ = self.get_centres(matrice_distances)
        degre_max = -1
        nouvelle_liste_centres = []
        for centre in liste_centres:
            degre = self.get_degre_sommet(centre)
            if degre > degre_max:
                nouvelle_liste_centres = []  # on vide la liste si on avait commence a la remplir
                degre_max = degre
            if degre == degre_max:
                nouvelle_liste_centres.append(centre)
        return len(nouvelle_liste_centres), nouvelle_liste_centres, degre_max

    def afficher_matrice_distances(self, matrice_distances):
        for sommet in self.liste_sommets:
            print("\t" + sommet.nom, end='')
        print("\n", end='')
        for sommetA in self.liste_sommets:
            print(sommetA.nom, end='')
            for sommetB in self.liste_sommets:
                print("\t" + str(matrice_distances[sommetA][sommetB]), end='')
            print("\n", end='')

# graphe1 = ListeAdjacence.graphe_aleatoire(9, 1)

# graphe1.afficher()

# matrice_distances = graphe1.get_distances()
# graphe1.afficher_matrice_distances(matrice_distances)
# print("diametre : " + str(graphe1.get_diametre(matrice_distances)))
# print("rayon : " + str(graphe1.get_rayon(matrice_distances)))
# _, centres, _ = graphe1.get_centres(matrice_distances)
# print("centres : " + " ".join([sommet.nom for sommet in centres]))
# _, centres_degre_max, deg_max = graphe1.get_centres_degre(matrice_distances)
# print("degre max : " + str(deg_max))
# print("centres de degre max : " + " ".join([sommet.nom for sommet in centres_degre_max]))
